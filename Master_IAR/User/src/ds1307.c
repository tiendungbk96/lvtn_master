
#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_i2c.h"
#include "ds1307.h"

void DS1307_Init (void)
{
        GPIO_InitTypeDef GPIO_InitStructure;
	I2C_InitTypeDef I2C_InitStructure;
	/* Thiet lap clock cho GPIO va I2C */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_DS1307_GPIO, ENABLE); 	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DS1307_I2C, ENABLE);
	/* Thiet lap GPIO */
	GPIO_InitStructure.GPIO_Pin = DS1307_SCL;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = DS1307_SDA;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	/* reset sate */
	//RCC_APB1PeriphResetCmd(RCC_APB1Periph_DS1307_I2C, ENABLE);
	/* Release I2C1 from reset state */
	//RCC_APB1PeriphResetCmd(RCC_APB1Periph_DS1307_I2C, DISABLE);
  /* Thiet lap che chay cho I2C */
	I2C_InitStructure.I2C_ClockSpeed = 100000;
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_OwnAddress1 = 0xD0;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
        
	I2C_Init(I2C_DS1307, &I2C_InitStructure);
	I2C_Cmd(I2C_DS1307,ENABLE);
}


uint8_t DS1307_ReadReg(uint8_t addReg)
{
	uint32_t timeOut = TIMEOUT;

	uint8_t tempValue;
	timeOut = TIMEOUT;
        while(I2C_GetFlagStatus(I2C_DS1307, I2C_FLAG_BUSY))
        {
          if((timeOut--) == 0) return 0;
        }
        
	
	I2C_GenerateSTART(I2C_DS1307, ENABLE);
        timeOut = TIMEOUT;
	while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_MODE_SELECT))
        {
          if((timeOut--) == 0) return 0;
        }

        I2C_Send7bitAddress(I2C_DS1307, 0xD0, I2C_Direction_Transmitter);
        timeOut = TIMEOUT;
        while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
        {
          if((timeOut--) == 0) return 0;
        } 
        I2C_SendData(I2C_DS1307, addReg);
        /*!< Test on EV8 and clear it */
        timeOut = TIMEOUT;
        while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_BYTE_TRANSMITTED))
        {
          if((timeOut--) == 0) return 0;
        }
	I2C_GenerateSTART(I2C_DS1307, ENABLE);
        timeOut = TIMEOUT;
	while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_MODE_SELECT))
        {
          if((timeOut--) == 0) return 0;
        }
        I2C_Send7bitAddress(I2C_DS1307, 0xD0, I2C_Direction_Receiver);
        timeOut = TIMEOUT;
        while(I2C_GetFlagStatus(I2C_DS1307, I2C_FLAG_ADDR) == RESET)
        {
          if((timeOut--) == 0) return 0;
        } 
        /*!< Disable Acknowledgement */
        I2C_AcknowledgeConfig(I2C_DS1307, DISABLE); 
        /* Clear ADDR register by reading SR1 then SR2 register (SR1 has already been read) */
        (void)I2C_DS1307->SR2;
        /*!< Send STOP Condition */
        I2C_GenerateSTOP(I2C_DS1307, ENABLE);
        timeOut = TIMEOUT;
        while(I2C_GetFlagStatus(I2C_DS1307, I2C_FLAG_RXNE) == RESET)
        {
          if((timeOut--) == 0) return 0;
        }
        tempValue = I2C_ReceiveData(I2C_DS1307);
        timeOut = TIMEOUT;
        while(I2C_DS1307->CR1 & I2C_CR1_STOP)
        {
          if((timeOut--) == 0) return 0;
        } 
        I2C_AcknowledgeConfig(I2C_DS1307, ENABLE);
        return tempValue;
}


uint8_t DS1307_SetTime(DS1307_Time_t* time)
{
  uint8_t data[7];
  data[0]=DEC2BCD(time->seconds);
  data[1]=DEC2BCD(time->minutes);
  data[2]=DEC2BCD(time->hours);
  data[3]=DEC2BCD(time->day);
  data[4]=DEC2BCD(time->date);
  data[5]=DEC2BCD(time->month);
  data[6]=DEC2BCD(time->year);
  
  uint32_t timeOut;
    
  timeOut = TIMEOUT;
  while(I2C_GetFlagStatus(I2C_DS1307, I2C_FLAG_BUSY))
  {
      if((timeOut--) == 0) return 0;
  }
  I2C_GenerateSTART(I2C_DS1307, ENABLE);
  while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_MODE_SELECT))
  {
      if((timeOut--) == 0) return 0;
  }  
  I2C_Send7bitAddress(I2C_DS1307, 0xD0, I2C_Direction_Transmitter);
        timeOut = TIMEOUT;
        while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
        {
          if((timeOut--) == 0) return 0;
        }


  //second address register
     I2C_SendData(I2C_DS1307, 0x00);
   timeOut = TIMEOUT;
   while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_BYTE_TRANSMITTING))
    {
          if((timeOut--) == 0) return 0;
    }

	
  /* write min */
    I2C_SendData(I2C_DS1307, data[0]);
	timeOut = TIMEOUT;
   while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_BYTE_TRANSMITTING))
    {
          if((timeOut--) == 0) return 0;
    }
  /* write min */
    I2C_SendData(I2C_DS1307, data[1]);
  /* Test on EV8 and clear it */
    /* Wait till all data have been physically transferred on the bus */
	timeOut = TIMEOUT;
   while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_BYTE_TRANSMITTING))
    {
          if((timeOut--) == 0) return 0;
    }
    /* write hour */
    I2C_SendData(I2C_DS1307, data[2]);
  /* Test on EV8 and clear it */
    /* Wait till all data have been physically transferred on the bus */
	timeOut = TIMEOUT;
   while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_BYTE_TRANSMITTING))
    {
          if((timeOut--) == 0) return 0;
    }
    /* write day */
    I2C_SendData(I2C_DS1307, data[3]);
  /* Test on EV8 and clear it */
    /* Wait till all data have been physically transferred on the bus */
	timeOut = TIMEOUT;
   while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_BYTE_TRANSMITTING))
    {
          if((timeOut--) == 0) return 0;
    }
    /* write date */
    I2C_SendData(I2C_DS1307, data[4]);
  /* Test on EV8 and clear it */
    /* Wait till all data have been physically transferred on the bus */
	timeOut = TIMEOUT;
   while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_BYTE_TRANSMITTING))
    {
          if((timeOut--) == 0) return 0;
    }
    /* write month */
    I2C_SendData(I2C_DS1307, data[5]);
  /* Test on EV8 and clear it */
    /* Wait till all data have been physically transferred on the bus */
	timeOut = TIMEOUT;
   while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_BYTE_TRANSMITTING))
    {
          if((timeOut--) == 0) return 0;
    }
    /* write year */
   
   I2C_SendData(I2C_DS1307, data[6]);
   	timeOut = TIMEOUT;
   while(!I2C_CheckEvent(I2C_DS1307, I2C_EVENT_MASTER_BYTE_TRANSMITTED))
    {
          if((timeOut--) == 0) return 0;
    }
   
   (void)I2C_DS1307->SR2;
   I2C_GenerateSTOP(I2C_DS1307, ENABLE);
 
        
    


   timeOut = TIMEOUT;
        while(I2C_DS1307->CR1 & I2C_CR1_STOP)
        {
          if((timeOut--) == 0) return 0;
        } 
   return 1;
}


/* @brief: Giai ma so BCD thanh so thap phan
 * @input: data de giai nen
 * @output: gia tri thap phan
 */
uint8_t BCD2DEC(uint8_t dat)
{
	uint8_t low;
	uint8_t high;
	low = dat & 0x0F;
	high = (dat >> 4)*10;
	return high+low;
}
/* @brief: Giai ma so thap phan thanh so BCD
 * @input: data de giai nen o dang so thap phan
 * @output: gia tri BCD
 */
uint8_t DEC2BCD(uint8_t dat)
{
	uint8_t low;
	uint8_t high;
	low = dat%10;
        low &= 0x0F;
	high =(dat/10)<<4;
        high &= 0xF0;
	return (high | low);
}

