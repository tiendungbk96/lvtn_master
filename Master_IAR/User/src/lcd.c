/*
*************************************************************************************************************************************
*															INCLUDED FILES															*
*************************************************************************************************************************************
*/
#include "lcd.h"
#include "delay.h"

static void lcd_Init_HW(void);
static void LCD_Enable(void);
static void LCD_Send4Bit(unsigned char Data);
static void LCD_SendCommand(unsigned char command);

/**
  * @brief  	initializes I/O pins connected to LCD.
  * @param  	None
  * @retval 	None
  */
void lcd_Init_HW(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Enable GPIO clocks for LCD control pins */
	//RCC_APB2PeriphClockCmd(LCD_RS_GPIO_CLK | LCD_RW_GPIO_CLK | LCD_EN_GPIO_CLK, ENABLE);
	RCC_APB2PeriphClockCmd(LCD_RS_GPIO_CLK | LCD_EN_GPIO_CLK|LCD_D4_GPIO_CLK|LCD_D5_GPIO_CLK|LCD_D6_GPIO_CLK|LCD_D7_GPIO_CLK, ENABLE);
	

	
	/* initialize LCD control lines to output */
	GPIO_InitStructure.GPIO_Pin 	= LCD_RS_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_50MHz;
//	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL; 
	GPIO_Init(LCD_RS_GPIO_PORT, &GPIO_InitStructure); 
	
	//GPIO_Init(LCD_RW_GPIO_PORT, &GPIO_InitStructure); 
	GPIO_InitStructure.GPIO_Pin 	= LCD_EN_GPIO_PIN;
	GPIO_Init(LCD_EN_GPIO_PORT, &GPIO_InitStructure); 
	
	GPIO_InitStructure.GPIO_Pin 	= LCD_D4_GPIO_PIN;
	GPIO_Init(LCD_D4_GPIO_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin 	= LCD_D5_GPIO_PIN;
	GPIO_Init(LCD_D5_GPIO_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin 	= LCD_D6_GPIO_PIN;
	GPIO_Init(LCD_D6_GPIO_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin 	= LCD_D7_GPIO_PIN;
	GPIO_Init(LCD_D7_GPIO_PORT, &GPIO_InitStructure);	
}
void LCD_Enable(void)
{
  GPIO_WriteBit(LCD_EN_GPIO_PORT,LCD_EN_GPIO_PIN,Bit_SET);
  DelayMs(1);
  GPIO_WriteBit(LCD_EN_GPIO_PORT,LCD_EN_GPIO_PIN,Bit_RESET);	
  DelayMs(1);	
}

void LCD_Send4Bit(unsigned char Data)
{
  if((Data&0x01) == 0)
  {
      GPIO_ResetBits(LCD_D4_GPIO_PORT,LCD_D4_GPIO_PIN);
  }
  if((Data&0x01) != 0)
  {
      GPIO_SetBits(LCD_D4_GPIO_PORT,LCD_D4_GPIO_PIN);
  }
  //
  if(((Data>>1)&0x01) == 0)
  {
      GPIO_ResetBits(LCD_D5_GPIO_PORT,LCD_D5_GPIO_PIN);
  }
  if(((Data>>1)&0x01) != 0)
  {
      GPIO_SetBits(LCD_D5_GPIO_PORT,LCD_D5_GPIO_PIN);
  }
  //
  if(((Data>>2)&0x01) == 0)
  {
     GPIO_ResetBits(LCD_D6_GPIO_PORT,LCD_D6_GPIO_PIN);
  }
  if(((Data>>2)&0x01) != 0)
  {
      GPIO_SetBits(LCD_D6_GPIO_PORT,LCD_D6_GPIO_PIN);
  }
  //
  if(((Data>>3)&0x01) == 0)
  {
      GPIO_ResetBits(LCD_D7_GPIO_PORT,LCD_D7_GPIO_PIN);
  }
  if(((Data>>3)&0x01) != 0)
  {
       GPIO_SetBits(LCD_D7_GPIO_PORT,LCD_D7_GPIO_PIN);
  }
  
  
 	
}

void LCD_SendCommand(unsigned char command)
{
	LCD_Send4Bit(command >>4);
	LCD_Enable();
	LCD_Send4Bit(command);
	LCD_Enable();
}


/*
*************************************************************************************************************************************
*															GLOBAL FUNCTIONS														*
*************************************************************************************************************************************
*/
/**
  * @brief  	Initializes the LCD.
  * @param  	None
  * @retval 	None
  */
void lcd_Init(void)
{
	/* initialize hardware */
	lcd_Init_HW();
        
        LCD_Send4Bit(0x00);
        DelayMs(20);
        GPIO_ResetBits(LCD_RS_GPIO_PORT,LCD_RS_GPIO_PIN);
        LCD_Send4Bit(0x03);
        LCD_Enable();
        DelayMs(5);
        LCD_Enable();
        DelayMs(1);
	LCD_Enable();
	LCD_Send4Bit(0x02);
	LCD_Enable();
	LCD_SendCommand(0x28);
	LCD_SendCommand(0x0C);
	LCD_SendCommand(0x06);
	LCD_SendCommand(0x01);
        DelayMs(5);
}

void LCD_Gotoxy(unsigned char x, unsigned char y)
{
  uint8_t  address;
	if(!y)
		address = (0x80+x);
	else
		address = (0xC0+x);
	
    LCD_SendCommand(address);
}

void LCD_PutChar(unsigned char Data)
{
    GPIO_WriteBit(LCD_RS_GPIO_PORT,LCD_RS_GPIO_PIN,Bit_SET);
 	LCD_SendCommand(Data);
    GPIO_WriteBit(LCD_RS_GPIO_PORT,LCD_RS_GPIO_PIN,Bit_RESET);
}

void LCD_Puts(char *s)
{
   	while (*s){
      	LCD_PutChar(*s);
     	s++;
   	}
}

void LCD_Clear(void)
{
 	LCD_SendCommand(0x01);  
        DelayMs(1);	
}