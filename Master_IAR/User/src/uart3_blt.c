/**
  ******************************************************************************
  * @file     bluetooth_interface.h
  * @author  Nguyentaihau
  * @version 
  * @date    19-10-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

/* Includes ------------------------------------------------------------------*/

#include "uart3_blt.h"
#include "delay.h"
#include "user_define.h"


uint8_t uart3_blt_Shiff_frame[FRAME_BLT_LEN];

static uint8_t uart3_blt_getFrame(uint8_t *pData, uint8_t CharterStart, uint8_t CharterStop, uint8_t len);
/*******************************************************************************
* Function name: internet_init
* Description:
* Input: 
* Return:
*******************************************************************************/
void uart3_blt_init(void)
{
  UART3_Bluetooth_Init();

}

void uart3_blt_send(uint8_t *pdata, uint8_t len)
{
  uint8_t dumy[2] = {0,0};
  UART3_Bluetooth_write(dumy, 2);
  UART3_Bluetooth_write(pdata, len);
}

static uint8_t uart3_blt_getFrame(uint8_t *pData, uint8_t CharterStart, uint8_t CharterStop, uint8_t len)
{
  uint8_t Read_status;
  uint8_t Frame_status;
  uint8_t Data_val ;
  uint8_t i,k;

  Frame_status = 0;
  // lay data tu Buffer va kiem tra xem co Frame gui ve khong.
  while((RxUart3.Len > 0)&&(Frame_status == 0)){
      Read_status = Rd_buff(&RxUart3,RxBuffUART_size, &Data_val);
      // kiem tra data new
      if(Read_status == 1){
        // Shiff toan bo data sang phai 1 byte
        for( i = 0;i < len;i++){
           uart3_blt_Shiff_frame[len - i -1] = uart3_blt_Shiff_frame[len - i - 2];
        }
        // day data moi vao
        uart3_blt_Shiff_frame[0] = Data_val;
        // kiem tra Frame dung la 1 frame
            if((uart3_blt_Shiff_frame[len-1] == CharterStart) && (uart3_blt_Shiff_frame[0] == CharterStop)){
              // truyen frame len tren
              for( k = 0; k < len;k++){
                *pData = uart3_blt_Shiff_frame[len - k - 1];
                pData++;
                uart3_blt_Shiff_frame[len - k - 1] = 0;
              }
              Frame_status = 1;
            }
            else {Frame_status = 0;}
      }
  }
  // return 1 neu co Frame, 0 neu khong co Frame.
  return Frame_status;
}


