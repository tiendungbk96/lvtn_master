#ifndef __DS1307_H
#define __DS1307_H

#define TIMEOUT    (uint32_t) 100000

/* dinh nghia giao tiep giua ds1307 va stm32f10x qua I2C */
#define I2C_DS1307		I2C1
#define DS1307_SCL		GPIO_Pin_6
#define DS1307_SDA		GPIO_Pin_7
#define RCC_APB2Periph_DS1307_GPIO		RCC_APB2Periph_GPIOB
#define RCC_APB1Periph_DS1307_I2C		RCC_APB1Periph_I2C1
/* Dinh nghia cac thanh ghi cua ds1307 */
#define DS1307_ADDRESS_READ	0xD1
#define DS1307_ADDRESS_WRITE	0xD0
#define DS1307_SECOND_REG 		0x00
#define DS1307_MINUTE_REG 		0x01
#define DS1307_HOURS_REG 		0x02
#define DS1307_DAY_REG				0x03
#define DS1307_DATE_REG			0x04
#define DS1307_MONTH_REG			0x05
#define DS1307_YEAR_REG			0x06
#define DS1307_CONTROL_REG		0x07


typedef struct {
   uint8_t seconds;   //Seconds,          00-59
   uint8_t minutes;   //Minutes,          00-59
   uint8_t hours;      //Hours,         00-23
   uint8_t day;      //Day in a week,    1-7
   uint8_t date;      //Day in a month   1-31
   uint8_t month;      //Month,         1-12
   uint8_t year;      //Year            00-99
} DS1307_Time_t;

  void DS1307_Init (void);

  uint8_t DS1307_ReadReg(uint8_t addReg);
  uint8_t BCD2DEC(uint8_t dat);
  uint8_t DEC2BCD(uint8_t dat);
  
  uint8_t DS1307_SetTime(DS1307_Time_t* time);

	 /*-----------------------------------------------------------------------------------------------*/

#endif /* __DS1307_ */