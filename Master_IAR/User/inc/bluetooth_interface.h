/**
  ******************************************************************************
  * @file     Uart2_internet.h
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

#ifndef __BLUETOOTH_INTERFACE_H
#define __BLUETOOTH_INTERFACE_H
/* Includes ------------------------------------------------------------------*/
#include "uart3_bluetooth.h"


/* Private typedef -----------------------------------------------------------*/
typedef enum{
  APP_RX_IDLE            = (uint8_t) 0,
  APP_RX_PARSER          = (uint8_t) 1
}app_state_t;


/* Private define ------------------------------------------------------------*/

void app_init(void);
app_state_t App_State(app_state_t curr_state, app_state_t *old_state);
void  App_Poll(void);
uint8_t app_getFrame(uint8_t *pData, uint8_t CharterStart, uint8_t CharterStop, uint8_t len);



#endif