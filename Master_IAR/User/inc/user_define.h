/**
  ******************************************************************************
  * @file     user_define.h
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

#ifndef __USER_DEFINE_H
#define __USER_DEFINE_H

typedef struct 
{
  uint8_t TB1;
  uint8_t TB2;
  uint8_t TB3;
  uint8_t TB4;
}status_device_t;




#endif