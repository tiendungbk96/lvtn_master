/**
  ******************************************************************************
  * @file     system_interface.h
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

#ifndef __SYSTEM_INTERFACE
#define __SYSTEM_INTERFACE
/* Includes ------------------------------------------------------------------*/
#include "uart1_system.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
typedef enum{
  SYS_RX_IDLE            = (uint8_t) 0,
  SYS_RX_PARSER          = (uint8_t) 1
}sys_state_t;



void System_Init(void);
sys_state_t App_Sytem_State(sys_state_t curr_state,
                                sys_state_t *old_state);
void  App_System_Poll(void);
uint8_t Sys_getFrame(uint8_t *pData, uint8_t CharterStart, uint8_t CharterStop, uint8_t len);
void sendRS485(uint8_t *iData, uint8_t NumByte);

#endif