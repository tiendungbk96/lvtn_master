/**
  ******************************************************************************
  * @file     Uart2_internet.h
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

#ifndef __UART_RXBUFF_H
#define __UART_RXBUFF_H
/* Includes ------------------------------------------------------------------*/

#include "stm32f10x.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
  #define  RxBuffUART_size  (uint8_t) 50

/* Private macro -------------------------------------------------------------*/
typedef struct{
    uint8_t Idx;
    uint8_t Len;
    uint8_t RxBuff[RxBuffUART_size];
}RxBuffUart_T;

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

uint8_t Wr_buff(RxBuffUart_T *buf_prt,uint8_t buf_size, uint8_t val);
uint8_t Rd_buff(RxBuffUart_T *buf_prt,uint8_t buf_size, __IO uint8_t *oval_prt);


#endif