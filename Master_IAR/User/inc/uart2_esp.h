/**
  ******************************************************************************
  * @file     internet_interface.h
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

#ifndef __UART2_ESP_H_
#define __UART2_ESP_H_
/* Includes ------------------------------------------------------------------*/
#include "uart2_internet.h"
/* Private typedef -----------------------------------------------------------*/


#define FRAME_ESP_LEN 17

void Uart2_esp_init(void);
void Uart2_esp_send(uint8_t *pdata, uint8_t len);

#endif